import React, { useState } from "react";
import ToDoForm from "./ToDoForm";
import ToDo from "./ToDo";

function ToDoList() {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    if (!todo.text || /^\s*$/.test(todo.text)) {
      // for space when we added our plans and  will use it space
      return;
    }

    const newTodos = [todo, ...todos]; //spread op

    setTodos(newTodos);
  };

  const removeTodo = (id) => {

    
    //*************** Remove to do ******************** */

    //await axios.delete("ENDPOINT?id="+ ${id}).then((result) => {
    //   console.log(result.data);
    // });

    const removeArr = [...todos].filter((todo) => todo.id !== id);
    setTodos(removeArr);
  };

  const updateTodo = (todoId, newValue) => {

    //*************** Update to do ******************** */

    // let article = {title : "Some title", id=5}

    //await axios.put("ENDPOINT", article).then((result) => {
    //   console.log(result.data);
    // });



    if (!newValue.text || /^\s*$/.test(newValue.text)) {
      // for space when we added our plans and  will use it space
      return;
    }

    setTodos((prev) =>
      prev.map((item) => (item.id === todoId ? newValue : item))
    );
  };

  const completeTodo = (id) => {
    let updatedTodos = todos.map((todo) => {
      if (todo.id === id) {
        todo.isComplete = !todo.isComplete;
      }
      return todo;
    });

    setTodos(updatedTodos);
  };

  return (
    <div>
      <h1> Write your plans for today</h1>
      <ToDoForm onSubmit={addTodo} />
      <ToDo
        todos={todos}
        completeTodo={completeTodo}
        removeTodo={removeTodo}
        updateTodo={updateTodo}
      />
    </div>
  );
}

export default ToDoList;
