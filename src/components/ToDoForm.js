import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

function ToDoForm(props) {
  useEffect(() => {
    alert("Getting data");
    //*************** GET list ******************** */

    // await axios.get("ENDPOINT").then((result) => {
    //   console.log(result.data);
    // });
  }, []);

  const [input, setInput] = useState(props.edit ? props.edit.value : "");

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    //*************** Create new to do ******************** */

    // let article = {title : "Some title"}

    // await axios.post("ENDPOINT", article).then((result) => {
    //   console.log(result.data);
    // });

    props.onSubmit({
      id: Math.floor(Math.random() * 10000),
      text: input,
    });

    setInput("");
  };
  return (
    <form className="todo-form" onSubmit={handleSubmit}>
      {props.edit ? (
        <>
          <input
            type="text"
            placeholder="Add new to do"
            value={input}
            name="text"
            className="todo-input"
            onChange={handleChange}
            ref={inputRef}
          />
          <button className="todo-button">Update button</button>
        </>
      ) : (
        <>
          <input
            type="text"
            placeholder="Add new to do"
            value={input}
            name="text"
            className="todo-input"
            onChange={handleChange}
            ref={inputRef}
          />
          <button className="todo-button">Add button</button>
        </>
      )}
    </form>
  );
}

export default ToDoForm;


