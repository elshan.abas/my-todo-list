import React from "react";
import './App.css';
import ToDoList from './components/ToDoList';

function App() {

  document.addEventListener("DOMContentLoaded", function(event) { 
    //we ready baby xd
  });

  return (
    <div className="todo-app">
      <ToDoList />
    </div>
  );
}

export default App;
